# frozen_string_literal: true

control 'stooj_dotfiles_work_workstation qutebrowser quickmarks file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/qutebrowser/quickmarks') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('weechat-urls http://127.0.1.1:60211') }
  end
end

control 'stooj_dotfiles_work_workstation qutebrowser bookmarks file' do
  title 'should match desired lines'

  describe file('/home/stooj/.config/qutebrowser/bookmarks/urls') do
    it { should be_file }
    it { should be_owned_by 'stooj' }
    it { should be_grouped_into 'stooj' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('https://regex101.com/ Online regex tester and debugger: PHP, PCRE, Python, Golang and JavaScript') }
  end
end

profiles = ['default']
profiles.each do |profile|
  control 'stooj_dotfiles_work_workstation qutebrowser ' + profile + ' launch script' do
    title 'should exist'

    describe file('/home/stooj/bin/' + profile + '-qutebrowser') do
      it { should be_file }
      it { should be_owned_by 'stooj' }
      it { should be_grouped_into 'stooj' }
      its('content') { should include('# Your changes will be overwritten.') }
      its('content') { should include('$QUTEBROWSER -r ' + profile) }
    end
  end
end
