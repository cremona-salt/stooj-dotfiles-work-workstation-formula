# -*- coding: utf-8 -*-
# vim: ft=yaml
---
stooj_dotfiles_workstation_common:
  code:
    salt_repos:
      template-formula:
        origin: https://github.com/saltstack-formulas/template-formula.git
        gitlab: git@gitlab.com:stooj/template-formula.git
  yubikey:
    trusted_keys:
      - 6AC6A4C2
  gpg:
    default_key: 6AC6A4C2
    encrypt_to: 6AC6A4C2
  pass:
    pass_stores:
      kitchen:
        repo: https://github.com/saltstack-formulas/template-formula.git
        target: /home/stooj/.kitchen-pass-store
        alias: kitchen-pass
  qutebrowser:
    python_script_repo: https://gitlab.com/stooj/qutebrowser-python-scripts.git
